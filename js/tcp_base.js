'use strict'

const PORT = 8888
const net = require('net')

const server = net.createServer()

server.on('connection', (client) => {
  console.log('client connected')
  console.log(client.address())
  client.on('end', () => console.log('client disconnected'))
  client.on('data', (data) => {
    console.log('received buffer: ' + data)
    const dataAsString = data.toString('utf8')
    console.log('decoded string: ' + dataAsString)
    client.write('server received:\n' + dataAsString)
    client.end()
  })

})

server.on('error', (err) => {throw err})

server.listen(PORT, '127.0.0.1', () => console.log(`server bound to port ${PORT}`))
