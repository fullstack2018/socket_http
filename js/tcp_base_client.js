'use strict'

const HOST = '127.0.0.1'
const PORT = 8888
const net = require('net')

const client = new net.Socket()

client.connect(PORT, HOST, () => {
  console.log('client connected !')
  client.write('Kawabungaaaaç !')
  client.end()
})

client.on('data', (data) => {
  console.log('Received: ' + data)
})

client.on('close', ()=> console.log('Connection closed'))
