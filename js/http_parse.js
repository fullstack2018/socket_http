const httpParse = (req) => {
  let requestObject = {}
  const lines = req.split('\n')
  const first = lines[0].split(' ')
  requestObject.verb = first[0]
  requestObject.path = first[1]
  requestObject.version = first[2].split('/')[1]
  requestObject.headers = {}
  return requestObject
}
