import threading
import socket

SERVER_ADDRESS = (HOST, PORT) = '', 8888
REQUEST_QUEUE_SIZE = 5

class ClientThread(threading.Thread):

    def __init__(self, ip, port, client_connection):

        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = client_connection
        print("[+] New thread for %s %s" % (self.ip, self.port, ))

    def run(self): 
        print("Connection from %s %s" % (self.ip, self.port, ))
        request = self.socket.recv(1024)
        print(request.decode())
        http_response = b"""\
        HTTP/1.1 200 OK
    
        Hello, World!
        """
        self.socket.sendall(http_response)
        self.socket.close()

def serve_forever():
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listen_socket.bind(SERVER_ADDRESS)
    listen_socket.listen(REQUEST_QUEUE_SIZE)
    print('Serving HTTP on port {port} ...'.format(port=PORT))

    while True:
        client_connection, (ip, port) = listen_socket.accept()
        newthread = ClientThread(ip, port, client_connection)
        newthread.start()

if __name__ == '__main__':
    serve_forever()
