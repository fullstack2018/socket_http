#!/usr/bin/env python3
import socket

HOST, PORT = '', 8888

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((HOST, PORT))

print("Client connected on {}:{}".format(HOST, PORT))

request = "Kawwwabungaaaa !"
socket.send(request.encode('utf-8'))
response = socket.recv(1024)

print(response.decode('utf-8'))
socket.close()
